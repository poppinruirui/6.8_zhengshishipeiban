﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComoList : MonoBehaviour {

	public GameObject m_preListItem;
	public static Vector3 vecTempPos = new Vector3 ();
	public static Vector3 vecTempScale = new Vector3 ();

	List<ListItem> m_lstItems = new List<ListItem>();
	public GameObject m_goItemsContainer;
	public GameObject m_goSelectedKuangKuang;

	public float c_ItemGapHeight = 36;
	public float c_ItemHeight = 24;
	public float c_ItemWidth = 24;

	public const float c_SelectedKuangKuangPosX = 153f;

	ListItem m_itemCurSelectedItem = null;

	// Use this for initialization
	void Start () {
		RectTransform rt = m_goSelectedKuangKuang.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 ( c_ItemWidth, c_ItemHeight );
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void AddItem( string szContent )
	{
		GameObject go = GameObject.Instantiate ( m_preListItem );
		ListItem item = go.GetComponent<ListItem> ();

		item.SetSize ( c_ItemWidth, c_ItemHeight );
		item._MyList = this;
		item.SetIndex ( m_lstItems.Count );
		m_lstItems.Add ( item );
		//item.transform.parent = m_goItemsContainer.transform;
		item.transform.SetParent( m_goItemsContainer.transform );
		RectTransform rt = go.GetComponent<RectTransform> ();
		vecTempScale.x = 1f;
		vecTempScale.y = 1f;
		vecTempScale.z = 1f;
		rt.localScale = vecTempScale;
		item.SetContent ( szContent );
		vecTempPos.x = 0;
		vecTempPos.y = -c_ItemGapHeight * item.GetIndex();
		vecTempPos.z = 0;
		item.transform.localPosition = vecTempPos;
	}

	public void Select( ListItem item )
	{
		DoSelect (item);
	}

	public void SelectByIndex( int nIndex )
	{
		if (nIndex < 0 || nIndex >= m_lstItems.Count) {
			return;
		}
		DoSelect ( m_lstItems[nIndex] );
	}

	public delegate void DelegateMethod_OnSelected( int nIndex );
	public DelegateMethod_OnSelected delegateMethodOnSelect;   //声明了一个Delegate对象


	public void DoSelect( ListItem item )
	{
		m_itemCurSelectedItem = item;
		UpdateKuangKuang ( item.GetIndex() );
		if (delegateMethodOnSelect != null) {
			delegateMethodOnSelect ( item.GetIndex() );
		}
	}

	void UpdateKuangKuang( int nIndex )
	{
		m_goSelectedKuangKuang.SetActive ( true );
		vecTempPos.x = 0;
		vecTempPos.y = -c_ItemGapHeight * nIndex;
		vecTempPos.z = 0;
		m_goSelectedKuangKuang.transform.localPosition = vecTempPos;
	}

	public ListItem GetCurSelectedItem()
	{
		return m_itemCurSelectedItem;
	}

	public string GeCurSelectedItemContent()
	{
		if (m_itemCurSelectedItem) {  
			return m_itemCurSelectedItem.GetContent ();
		}
		return "";
	}

	public void ClearAll()
	{
		for (int i = m_lstItems.Count - 1; i >= 0; i--) {
			GameObject.Destroy ( m_lstItems[i].gameObject );
		}
		m_lstItems.Clear ();
	}

	public int GetCount()
	{
		return m_lstItems.Count;
	}
}

