﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CBoWenGroup : MonoBehaviour
{
    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    // Use this for initialization
    void Start()
    {
        for ( int i = 0; i < 10; i++ )
        {
            CBoWen bowen = GameObject.Instantiate(CStripeManager.s_Instance.m_preBoWen).GetComponent<CBoWen>();
            vecTempPos.x = -0.1f * i;
            vecTempPos.y = -0.1f * i;
            vecTempPos.z = 0f;
            vecTempScale.x = 0.1f;
            vecTempScale.y = 0.1f;
            vecTempScale.z = 1f;
           
            bowen.SetColor(CStripeManager.s_Instance.m_colorBoWen);
            bowen.transform.SetParent( this.transform );
            bowen.transform.localPosition = vecTempPos;
            bowen.transform.localScale = vecTempScale;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void SetColor(Color color)
    {
        foreach( Transform child in this.transform )
        {
            CBoWen bowen = child.gameObject.GetComponent<CBoWen>();
            bowen.SetColor(color);
        }

    }

   
}
