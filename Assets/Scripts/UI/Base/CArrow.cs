﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CArrow : MonoBehaviour {

    public bool m_bUI = false;

    public SpriteRenderer _srMain;
    public Image _imgMain;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetColor( Color color )
    {
        if (m_bUI)
        {
            _imgMain.color = color;
        }
        else
        {
            _srMain.color = color;
        }
    }

    
}
