﻿/* 战争迷雾
 * 这里面用到的尺寸都是屏幕坐标哦，不是世界坐标
   屏幕左下角是（-0.5,-0.5），屏幕右上角是（0.5，0.5）
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CWarFog : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static CWarFog s_Instance;
    public GameObject m_goWarFog;

    public Material m_matFog;

    float m_fWorldSzie = 1;
    float m_fHalfWorldSizeX = 1;
    float m_fHalfWorldSizeY = 1;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        for (int i = 0; i < Main.MAX_BALL_NUM; i++)
        {
            m_lstBallRadius.Add(0);
            m_lstBallPosX.Add(0);
            m_lstBallPosY.Add(0);
        }
        m_matFog.SetFloatArray("_aryBallsRadius", m_lstBallRadius);
        m_matFog.SetFloatArray("_aryBallsPosX", m_lstBallPosX);
        m_matFog.SetFloatArray("_aryBallsPosY", m_lstBallPosY);
    }
	
	// Update is called once per frame
	void Update () {
       UpdateFog();

    }

    public void SetWorldInfo( float fClassCircleRadius, float fWorldSizeX, float fWorldSizeY)
    {
        m_fHalfWorldSizeX = fWorldSizeX / 2f;
        m_fHalfWorldSizeY = fWorldSizeY / 2f;
        m_fWorldSzie = fWorldSizeX > fWorldSizeY? fWorldSizeX : fWorldSizeY;
        float fScreenRadius = fClassCircleRadius / m_fWorldSzie;
        m_matFog.SetFloat("_ClassCircleRadius", fScreenRadius);
        vecTempScale.x = m_fWorldSzie;
        vecTempScale.y = m_fWorldSzie;
        vecTempScale.z = 1f;
        m_goWarFog.transform.localScale = vecTempScale;
    }

    public struct eBallInfo
    {
        public float radius;
        public float posx;
        public float posy;
    };

    List<float> m_lstBallRadius = new List<float>();
    List<float> m_lstBallPosX = new List<float>();
    List<float> m_lstBallPosY = new List<float>();

    void UpdateFog()
    {
        if (Main.s_Instance == null || Main.s_Instance.m_MainPlayer == null)
        {
            return;
        }

        m_lstBallRadius.Clear();
        m_lstBallPosX.Clear();
        m_lstBallPosY.Clear();

        bool bDark = false;
        List<Ball> lstBalls = Main.s_Instance.m_MainPlayer.GetBallList();
        for ( int i = 0; i < lstBalls.Count; i++ )
        {
            Ball ball = lstBalls[i];
            if ( ball.IsDead() )
            {
                continue;
            }

            if ( ball.GetClassId() == 1 )
            {
                bDark = true;
            }

            float radius = ball.GetRadius() / m_fWorldSzie;
            m_lstBallRadius.Add(radius * 2f );
            float posx = ( ball.GetPos().x / m_fHalfWorldSizeX ) * 0.5f;
            m_lstBallPosX.Add(posx);
            float posy = ( ball.GetPos().y / m_fHalfWorldSizeY ) * 0.5f;
            m_lstBallPosY.Add(posy);

        } // end for

        if (!bDark)
        {
            m_goWarFog.SetActive(false );
            return;
        }
        m_goWarFog.SetActive(true);

        if (m_lstBallRadius.Count > 0)
        {
            m_matFog.SetInt("_CurLiveBallNum", m_lstBallRadius.Count);
            m_matFog.SetFloatArray("_aryBallsRadius", m_lstBallRadius);
            m_matFog.SetFloatArray("_aryBallsPosX", m_lstBallPosX);
            m_matFog.SetFloatArray("_aryBallsPosY", m_lstBallPosY);
        }
    }
}
